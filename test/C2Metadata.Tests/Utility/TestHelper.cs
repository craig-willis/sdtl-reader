﻿using C2Metadata.Common.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace C2Metadata.SpssToSdtl.Tests.Utility
{
    public static class TestHelper
    {

        public static void SaveJson(Program program, string fileName)
        {
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                json = json.Replace("\r\n", "\n");
                File.WriteAllText(fileName, json);
            }
        }

        public static void AssertSingleVariableWithName(VariableReferenceBase variableRef, string name)
        {
            Xunit.Assert.IsType<VariableSymbolExpression>(variableRef);

            var varSymbol = variableRef as VariableSymbolExpression;
            Xunit.Assert.Equal(name, varSymbol.VariableName);
        }

        public static void AssertSingleVariableWithName(List<VariableReferenceBase> variables, string name)
        {
            Xunit.Assert.NotNull(variables);
            Xunit.Assert.Single(variables);

            Xunit.Assert.IsType<VariableSymbolExpression>(variables[0]);
            var varSymbol = variables[0] as VariableSymbolExpression;
            Xunit.Assert.Equal(name, varSymbol.VariableName);
        }
    }
}
