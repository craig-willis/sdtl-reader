﻿using Markdig;
using Markdig.Renderers;
using Markdig.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.RConverter
{
    public class RMarkdownToRConverter
    {
        public string Convert(string content)
        {
            var builder = new StringBuilder();
            var renderer = new RMarkdownRenderer(builder);
            var pipeline = new MarkdownPipelineBuilder().Build();
            pipeline.Setup(renderer);
            MarkdownDocument ast = Markdig.Markdown.Parse(content, pipeline);
            renderer.Render(ast);

            return builder.ToString();
        }
    }

    public class RMarkdownRenderer : RendererBase
    {
        StringBuilder builder;

        public RMarkdownRenderer(StringBuilder builder)
        {
            this.builder = builder;
            ObjectRenderers.Add(new CatchallRRenderer(builder));
        }

        public override object Render(MarkdownObject markdownObject)
        {
            Write(markdownObject);
            return builder;
        }
    }

    public class CatchallRRenderer : IMarkdownObjectRenderer
    {
        public CatchallRRenderer(StringBuilder builder)
        {
            Builder = builder;
        }

        public StringBuilder Builder { get; }

        public bool Accept(RendererBase renderer, MarkdownObject obj) => obj is LeafBlock;

        public void Write(RendererBase renderer, MarkdownObject objectToRender)
        {
            if (objectToRender is CodeBlock codeBlock)
            {
                WriteText(codeBlock, false);
            }
            else if (objectToRender is LeafBlock leafBlock)
            {
                WriteText(leafBlock, true);
            }
        }

        private void WriteText(LeafBlock block, bool isCommented)
        {
            if (block.Inline != null)
            {
                var inline = (Markdig.Syntax.Inlines.Inline) block.Inline;
                while (inline != null)
                {
                    // TODO Process inlines to get the full text
                    //string text = inline.ToString();
                    //if (isCommented)
                    //{
                    //    text = "# " + text;
                    //}

                    //Builder.AppendLine(text);
                    inline = inline.NextSibling;
                }

            }
            if (block.Lines.Lines != null)
            {
                var lines = block.Lines;
                var slices = lines.Lines;
                for (int i = 0; i < lines.Count; i++)
                {
                    var slice = slices[i].Slice;
                    if (slice.Start > slice.End)
                    {
                        return;
                    }

                    string text = GetText(slice.Text, slice.Start, slice.Length);
                    if (isCommented)
                    {
                        Builder.AppendLine("#" + text);
                    }
                    else
                    {
                        Builder.AppendLine(text);
                    }

                }
            }

        }

        private string GetText(string text, int start, int length)
        {
            var buffer = text.ToCharArray();
            return new string(buffer, start, length);
        }
    }
}
