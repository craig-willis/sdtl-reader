using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes the arguments in a function as specified in the SDTL Function Library.
    /// <summary>
    public partial class FunctionArgument
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public FunctionArgument() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The name of the parameter.
        /// <summary>
        public string ArgumentName { get; set; }
        /// <summary>
        /// The value of the parameter.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase ArgumentValue { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (ArgumentName != null)
            {
                xEl.Add(new XElement(ns + "ArgumentName", ArgumentName));
            }
            if (ArgumentValue != null) { xEl.Add(ArgumentValue.ToXml("ArgumentValue")); }
            return xEl;
        }
    }
}

