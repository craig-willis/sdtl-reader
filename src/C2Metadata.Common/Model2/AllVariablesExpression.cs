using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// An expression that represents all variables in the dataset, similar to `_all` in SPSS or Stata.
    /// 
    /// <summary>
    public partial class AllVariablesExpression : VariableReferenceBase
    {

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("VariableReferenceBase").Descendants())
            {
                xEl.Add(el);
            }
            return xEl;
        }
    }
}

