using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Combines datasets by concatenation for datasets with the same or overlapping variables.  
    /// 
    /// <summary>
    public partial class AppendDatasets : TransformBase
    {
        /// <summary>
        /// Description of files to be appended
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<AppendFileDescription> AppendFiles { get; set; } = new List<AppendFileDescription>();
        public bool ShouldSerializeAppendFiles() { return AppendFiles.Count > 0; }
        /// <summary>
        /// Creates a new variable identifying the source dataframe for a row with the variable name given in the value of the property
        /// <summary>
        public string AppendFlagVariable { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (AppendFiles != null && AppendFiles.Count > 0)
            {
                foreach (var item in AppendFiles)
                {
                    xEl.Add(item.ToXml("AppendFiles"));
                }
            }
            if (AppendFlagVariable != null)
            {
                xEl.Add(new XElement(ns + "AppendFlagVariable", AppendFlagVariable));
            }
            return xEl;
        }
    }
}

