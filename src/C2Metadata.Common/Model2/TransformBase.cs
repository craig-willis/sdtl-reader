using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TransformBase defines general properties available on all transform commands.
    /// 
    /// <summary>
    public abstract partial class TransformBase
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public TransformBase() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The type of transform command
        /// <summary>
        public string Command { get; set; }
        /// <summary>
        /// Information about the source of the transform command.
        /// <summary>
        public SourceInformation SourceInformation { get; set; }
        /// <summary>
        /// Signify the dataframe which this transform produces.
        /// <summary>
        public List<string> ProducesDataframe { get; set; } = new List<string>();
        public bool ShouldSerializeProducesDataframe() { return ProducesDataframe.Count > 0; }
        /// <summary>
        /// Signify the dataframe which this transform acts upon.
        /// <summary>
        public List<string> ConsumesDataframe { get; set; } = new List<string>();
        public bool ShouldSerializeConsumesDataframe() { return ConsumesDataframe.Count > 0; }
        /// <summary>
        /// Adds a message that can be displayed with the command.
        /// <summary>
        public List<string> MessageText { get; set; } = new List<string>();
        public bool ShouldSerializeMessageText() { return MessageText.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (Command != null)
            {
                xEl.Add(new XElement(ns + "Command", Command));
            }
            if (SourceInformation != null) { xEl.Add(SourceInformation.ToXml("SourceInformation")); }
            if (ProducesDataframe != null && ProducesDataframe.Count > 0)
            {
                xEl.Add(
                    from item in ProducesDataframe
                    select new XElement(ns + "ProducesDataframe", item.ToString()));
            }
            if (ConsumesDataframe != null && ConsumesDataframe.Count > 0)
            {
                xEl.Add(
                    from item in ConsumesDataframe
                    select new XElement(ns + "ConsumesDataframe", item.ToString()));
            }
            if (MessageText != null && MessageText.Count > 0)
            {
                xEl.Add(
                    from item in MessageText
                    select new XElement(ns + "MessageText", item.ToString()));
            }
            return xEl;
        }
    }
}

