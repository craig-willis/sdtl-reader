using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Defines a range of numeric values.
    /// 
    /// <summary>
    public partial class NumberRangeExpression : ExpressionBase
    {
        /// <summary>
        /// Starting value for range
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase NumberRangeStart { get; set; }
        /// <summary>
        /// Ending value for range
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase NumberRangeEnd { get; set; }
        /// <summary>
        /// Increment for stepping through range
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase NumberRangeIncrement { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (NumberRangeStart != null) { xEl.Add(NumberRangeStart.ToXml("NumberRangeStart")); }
            if (NumberRangeEnd != null) { xEl.Add(NumberRangeEnd.ToXml("NumberRangeEnd")); }
            if (NumberRangeIncrement != null) { xEl.Add(NumberRangeIncrement.ToXml("NumberRangeIncrement")); }
            return xEl;
        }
    }
}

