using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Creates a new empty dataframe.  Numbers of rows or columns may be specified.  All values are assumed to be missing.
    /// 
    /// <summary>
    public partial class NewDataframe : TransformBase
    {
        /// <summary>
        /// Number of rows in new dataframe
        /// <summary>
        public int NumberOfRows { get; set; }
        /// <summary>
        /// Number of columns in new dataframe
        /// <summary>
        public int NumberOfColumns { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            xEl.Add(new XElement(ns + "NumberOfRows", NumberOfRows));
            xEl.Add(new XElement(ns + "NumberOfColumns", NumberOfColumns));
            return xEl;
        }
    }
}

