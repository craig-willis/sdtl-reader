using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A group of expressions to be evaluated before expressions outside of the group.  Used to control the order of operations in a formula.
    /// 
    /// <summary>
    public partial class GroupedExpression : ExpressionBase
    {
        /// <summary>
        /// Expression to be evaluated.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase Expression { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Expression != null) { xEl.Add(Expression.ToXml("Expression")); }
            return xEl;
        }
    }
}

