using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Creates a new dataset with multiple rows per case by assigning a set of variables in the original dataset to a single variable in the new dataset.
    /// 
    /// <summary>
    public partial class ReshapeLong : TransformBase
    {
        /// <summary>
        /// New variables created by this command.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ReshapeItemDescription> MakeItems { get; set; } = new List<ReshapeItemDescription>();
        public bool ShouldSerializeMakeItems() { return MakeItems.Count > 0; }
        /// <summary>
        /// New variable identifying the case number in the wide data that created this row.
        /// <summary>
        public string CaseNumberVariable { get; set; }
        /// <summary>
        /// One or more variables identifying unique rows in the wide data.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase IDVariables { get; set; }
        /// <summary>
        /// Variables to be dropped from the new dataset.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase DropVariables { get; set; }
        /// <summary>
        /// Variables to be kept in the new dataset.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase KeepVariables { get; set; }
        /// <summary>
        /// When set to TRUE, rows in which all constructed variables are missing are not deleted.
        /// <summary>
        public bool KeepNullCases { get; set; }
        /// <summary>
        /// New variable with the number of cases in the long dataset that were created from the source row in the wide dataset.
        /// <summary>
        public string CountByID { get; set; }
        /// <summary>
        /// Label for the CountByID variable.
        /// <summary>
        public string CountByIDLabel { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (MakeItems != null && MakeItems.Count > 0)
            {
                foreach (var item in MakeItems)
                {
                    xEl.Add(item.ToXml("MakeItems"));
                }
            }
            if (CaseNumberVariable != null)
            {
                xEl.Add(new XElement(ns + "CaseNumberVariable", CaseNumberVariable));
            }
            if (IDVariables != null) { xEl.Add(IDVariables.ToXml("IDVariables")); }
            if (DropVariables != null) { xEl.Add(DropVariables.ToXml("DropVariables")); }
            if (KeepVariables != null) { xEl.Add(KeepVariables.ToXml("KeepVariables")); }
            xEl.Add(new XElement(ns + "KeepNullCases", KeepNullCases));
            if (CountByID != null)
            {
                xEl.Add(new XElement(ns + "CountByID", CountByID));
            }
            if (CountByIDLabel != null)
            {
                xEl.Add(new XElement(ns + "CountByIDLabel", CountByIDLabel));
            }
            return xEl;
        }
    }
}

