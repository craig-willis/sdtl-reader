using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A list of variables which may include variable names (VariableSymbolExpression, CompositeVariableNameExpression), variable ranges (VariableRangeExpression), and variable lists.
    /// 
    /// <summary>
    public partial class VariableListExpression : VariableReferenceBase
    {
        /// <summary>
        /// An array of variables and/or variable ranges
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> Variables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("VariableReferenceBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                foreach (var item in Variables)
                {
                    xEl.Add(item.ToXml("Variables"));
                }
            }
            return xEl;
        }
    }
}

