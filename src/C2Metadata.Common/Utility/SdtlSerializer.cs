﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Utility
{
    public static class SdtlSerializer
    {


        public static string SerializeAsJson(sdtl.Program program)
        {
            var settings = GetJsonSettings();
            string json = JsonConvert.SerializeObject(program, settings);
            return json;
        }

        public static string SerializeAsJson(sdtl.ItemContainer container)
        {
            JsonSerializerSettings settings = GetJsonSettings();
            string json = JsonConvert.SerializeObject(container, settings);
            return json;
        }

        private static JsonSerializerSettings GetJsonSettings()
        {
            return new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new BaseFirstContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };
        }
    }
}
