﻿using Eto.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdtlReader.Utility;
using sdtl;

namespace SdtlReader.CommandViews
{
    public class ExecuteControl : Panel
    {
        private Execute exec;
        private object cmd;

        public ExecuteControl(Execute exec)
        {
            this.exec = exec;

            Content = UIBuilder.MakeScrollable(
                UIBuilder.CreateKeyValueTable(
                    "Execute", string.Empty
                ),
                UIBuilder.CreateSourceInformationCells(exec.SourceInformation)
            );
        }
    }
}
