﻿using Eto.Forms;
using sdtl;
using SdtlReader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.Views
{
    public class TransformSequenceSummaryView : Panel
    {
        private ListBox varList;
        private Splitter splitter;

        public TransformSequenceSummaryView(Program program)
        {
            var commands = program.Commands;

            var groupedCommands = commands
                .GroupBy(x => x.Command)
                .OrderByDescending(x => x.Count());

            var commandCounts = new List<string>();
            var functionCounts = new List<string>();

            commandCounts.Add("Total");
            commandCounts.Add(commands.Count().ToString());

            foreach (var group in groupedCommands)
            {
                commandCounts.Add(group.Key);
                commandCounts.Add(group.Count().ToString());
            }

            var functionCounter = new FunctionCounter();
            var functionCountResults = functionCounter.CountFunctions(commands);

            foreach (var pair in functionCountResults)
            {
                functionCounts.Add(pair.Key);
                functionCounts.Add(pair.Value.ToString());
            }

            var tabControl = new TabControl();

            // Frequency page
            var freqPage = new TabPage();
            freqPage.Text = "Summary";
            tabControl.Pages.Add(freqPage);

            freqPage.Content = UIBuilder.MakeScrollable(UIBuilder.CreateKeyValueTable(commandCounts.ToArray()));

            // Function counts
            var functionPage = new TabPage();
            functionPage.Text = "Functions";
            tabControl.Pages.Add(functionPage);

            functionPage.Content = UIBuilder.MakeScrollable(UIBuilder.CreateKeyValueTable(functionCounts.ToArray()));

            // Variable page
            var varPage = new TabPage();
            varPage.Text = "Variables";
            tabControl.Pages.Add(varPage);

            varList = new ListBox();
            varList.Size = new Eto.Drawing.Size(300, 300);
            varList.SelectedIndexChanged += VarList_SelectedIndexChanged;

            var gatherer = new VariableGatherer();
            var variables = gatherer.GatherVariables(commands);

            foreach (var varGroup in variables.GroupBy(x => x.VariableName))
            {
                var item = new ListItem();
                item.Text = varGroup.Key;
                item.Tag = varGroup;
                varList.Items.Add(item);
            }

            splitter = new Splitter();
            splitter.Panel1 = varList;
            splitter.Panel2 = new Label { Text = "Select a variable to the left." };
            splitter.FixedPanel = SplitterFixedPanel.Panel1;
            varPage.Content = splitter;

            // Messages page
            var messagePage = new TabPage();
            messagePage.Text = "Messages";
            tabControl.Pages.Add(messagePage);

            var messageTextBox = new TextBox();
            messageTextBox.ReadOnly = true;
            messageTextBox.Text = string.Join("\n", program.Messages.Select(x => 
                $"{x.LineNumber}:{x.CharacterPosition} - {x.Severity} - {x.MessageText}"));

            messagePage.Content = messageTextBox;

            // Set the main content.
            Content = tabControl;
            
        }

        private void VarList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (varList.SelectedIndex < 0)
            {
                splitter.Panel2 = new Label { Text = "Select a variable to the left." };
                return;
            }

            var item = varList.Items[varList.SelectedIndex] as ListItem;
            if (item.Tag is IGrouping<string, VariableOperation> group)
            {
                var stack = new StackLayout();
                foreach (var op in group)
                {
                    stack.Items.Add(new Label { Text = op.Description });
                }

                splitter.Panel2 = UIBuilder.MakeScrollable(stack);
            }

        }
    }
}
