/*
 [The "BSD licence"]
 Copyright (c) 2013 Terence Parr
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*

This has been modified from the original.

- Target C# not Java.
- Don't skip over comments.
- Make walking the parse tree simpler.

*/

grammar R;

prog:   (   expr (';'|NL)*
        |   NL
        | COMMENT
        )*
        EOF
    ;

/*
expr_or_assign
    :   expr ('<-'|'='|'<<-') expr_or_assign
    |   expr
    ;
*/

expr:   expr '[[' sublist ']' ']'                   # singleIndexExpression
    |   expr '[' sublist ']'                        # multipleIndexExpression
    |   expr ('::'|':::') expr                      # namespaceAccessExpression
    |   expr ('$'|'@') expr                         # objectAccessExpression
    |   <assoc=right> expr '^' expr                 # exponentExpression
    |   '-' expr                                    # positiveExpression
    |   '+' expr                                    # negativeExpression
    |   expr ':' expr                               # rangeExpression
    |   <assoc=right> userOpLeft=expr USER_OP userOpRight=expr    # userOpExpression
    |   expr '*' expr                               # multiplyExpression
    |   expr '/' expr                               # divideExpression
    |   expr '+' expr                               # addExpression
    |   expr '-' expr                               # subtractExpression
    |   expr ('>'|'>='|'<'|'<='|'=='|'!=') expr     # comparisonExpression
    |   '!' expr                                    # notExpression
    |   expr ('&'|'&&') expr                        # andExpression
    |   expr ('|'|'||') expr                        # orExpression
    |   '~' expr                                    # rightSideOnlyModelFormulaExpression
    |   expr '~' expr                               # modelFormulaExpression
    |   functionName=ID '(' argumentList=sublist ')' # callFunctionExpression
    |   expr ('<-'|'<<-'|'='|'->'|'->>'|':=') expr  # assignmentExpression
    |   'function' '(' formlist? ')' expr           # defineFunctionExpression
    |   '{' exprlist '}'                            # compoundExpression
    |   'if' '(' expr ')' expr                      # ifExpression
    |   'if' '(' expr ')' expr 'else' expr          # ifElseExpression
    |   'for' '(' ID 'in' expr ')' expr             # forExpression
    |   'while' '(' expr ')' expr                   # whileExpression
    |   'repeat' expr                               # repeatExpression
    |   '?' expr                                    # getHelpExpression
    |   'next'                                      # nextExpression
    |   'break'                                     # breakExpression
    |   '(' expr ')'                                # parenthesizedExpression
    |   ID                                          # idExpression
    |   STRING                                      # stringExpression
    |   HEX                                         # hexExpression
    |   INT                                         # intExpression
    |   FLOAT                                       # floatExpression
    |   COMPLEX                                     # complexExpression
    |   'NULL'                                      # nullExpression
    |   'NA'                                        # naExpression
    |   'Inf'                                       # infExpression
    |   'NaN'                                       # nanExpression
    |   'TRUE'                                      # trueExpression
    |   'FALSE'                                     # falseExpression
    ;

exprlist
    :   expr ((';'|NL) expr?)*
    |
    ;

formlist : form (',' form)* ;

form:   ID
    |   ID '=' expr
    |   '...'
    |   '.'
    ;

sublist : argument+=sub (',' argument+=sub)* ;

sub :   
        ID '='                                  # leftSideOnlyAssignmentArgument
    |   variableName=ID '=' assignmentExpr=expr # assignmentArgument
    |   STRING '='                              # leftSideOnlyStringAssignmentArgument
    |   STRING '=' expr                         # stringAssignmentArgument
    |   'NULL' '='                              # leftSideOnlyNullAssignmentArgument
    |   'NULL' '=' expr                         # nullAssignmentArgument
    |   '...'                                   # ellipsisArgument
    |   '.'                                     # dotArgument
    |   expr                                    # expressionArgument
    |                                           # emptyArgument
    ;

HEX :   '0' ('x'|'X') HEXDIGIT+ [Ll]? ;

INT :   DIGIT+ [Ll]? ;

fragment
HEXDIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

FLOAT:  DIGIT+ '.' DIGIT* EXP? [Ll]?
    |   DIGIT+ EXP? [Ll]?
    |   '.' DIGIT+ EXP? [Ll]?
    ;
fragment
DIGIT:  '0'..'9' ; 
fragment
EXP :   ('E' | 'e') ('+' | '-')? INT ;

COMPLEX
    :   INT 'i'
    |   FLOAT 'i'
    ;

STRING
    :   '"' ( ESC | ~[\\"] )*? '"'
    |   '\'' ( ESC | ~[\\'] )*? '\''
    |   '`' ( ESC | ~[\\'] )*? '`'
    ;

fragment
ESC :   '\\' [abtnfrv"'\\]
    |   UNICODE_ESCAPE
    |   HEX_ESCAPE
    |   OCTAL_ESCAPE
    ;

fragment
UNICODE_ESCAPE
    :   '\\' 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT
    |   '\\' 'u' '{' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT '}'
    ;

fragment
OCTAL_ESCAPE
    :   '\\' [0-3] [0-7] [0-7]
    |   '\\' [0-7] [0-7]
    |   '\\' [0-7]
    ;

fragment
HEX_ESCAPE
    :   '\\' HEXDIGIT HEXDIGIT?
    ;

ID  :   '.' (LETTER|'_'|'.') (LETTER|DIGIT|'_'|'.')*
    |   LETTER (LETTER|DIGIT|'_'|'.')*
    ;
    
fragment LETTER  : [a-zA-Z] ;

USER_OP :   '%' .*? '%' ;

COMMENT :   '#' .*? '\r'? '\n';

// Match both UNIX and Windows newlines
NL      :   '\r'? '\n' ;

WS      :   [ \t\u000C]+ -> skip ;
