﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace C2Metadata.Web.Models
{
    public class DataToTranslate
    {
        [AllowHtml]
        public string data { get; set; }
    }
}
