"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
let ArrayComponent = 
//<ul>
//    <li * ngFor="let item of data" class="list" id = "listFormat" >
//    <a>item < /a><input type="checkbox">
//        < /li>
//        < /ul>
class ArrayComponent {
    constructor() {
        //obj: Object = JSON.parse(this.data);
        this.objectKeys = Object.keys;
    }
};
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ArrayComponent.prototype, "data", void 0);
ArrayComponent = __decorate([
    core_1.Component({
        selector: 'my-list',
        template: `
    <ul *ngIf="data != null" style="margin-left:-30px;">
        <li *ngFor="let key of objectKeys(data)" class="list" id="listFormat">
            <a *ngIf="key != null && data?.constructor.name != 'Array'" id="a_padding">{{ key }} : <a *ngIf="data[key] != null && data[key]?.constructor.name != 'Object' && data[key].constructor.name != 'Array'" style="text-decoration: none;">{{data[key]}}</a></a>
            <a *ngIf="key != null && data?.constructor.name == 'Array'" id="a_padding">items :  <a *ngIf="data[key] != null && data[key]?.constructor.name != 'Object' && data[key].constructor.name != 'Array'" style="text-decoration: none;">{{data[key]}}</a></a>
            <input *ngIf="key != null && data[key]?.constructor.name == 'Object'" type="checkbox">
            <ul *ngIf="key != null && data[key] != null && data[key]?.constructor.name == 'Object'">
                <li *ngFor="let k of objectKeys(data[key])" class="list" id="listFormat">
                    <a *ngIf="k != null" id="a_padding">{{k}} : <a *ngIf="k != null && data[key][k] != null && data[key][k]?.constructor.name != 'Object' && data[key][k].constructor.name != 'Array'" style="text-decoration: none;">{{data[key][k]}}</a></a>
                    <my-list *ngIf="k != null && data[key][k] != null && data[key][k]?.constructor.name == 'Array'" [data]="data[key][k]"></my-list>
                </li>
            </ul>
            <my-list *ngIf="key != null && data[key] != null && data[key]?.constructor.name == 'Array'" [data]="data[key]"></my-list>
        </li>
    </ul>
`
    })
    //<ul>
    //    <li * ngFor="let item of data" class="list" id = "listFormat" >
    //    <a>item < /a><input type="checkbox">
    //        < /li>
    //        < /ul>
    ,
    __metadata("design:paramtypes", [])
], ArrayComponent);
exports.ArrayComponent = ArrayComponent;
//# sourceMappingURL=array.component.js.map