﻿export interface SourceInfo {
    lineNumberStart: Number;
    lineNumberEnd: Number;
    sourceStartIndex: Number;
    sourceStopIndex: Number;
    originalSourceText: string;
}