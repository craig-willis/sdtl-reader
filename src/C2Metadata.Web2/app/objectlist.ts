﻿import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { Services } from './services/services';

@Component({
    selector: 'my-obj',
    template: `
        <ul>
            <li *ngFor="let key of objectKeys(data)" class="list">
                <a (click)='onClick(data[key])'>{{key}}</a>
                <my-obj *ngIf="data[key].constructor.name == 'Object'" [data]="data[key]"></my-obj>
            </li>
        </ul>
`
})
export class ObjectList {
    @Input() data: object;
    //obj: Object = JSON.parse(this.data);;
    objectKeys = Object.keys;
    //info: any;
    //@Output() notify = new EventEmitter<any>();
    constructor(public service: Services) {
    }

    isObj(val: any) {
        return typeof val === 'object';
    }

    onClick(info: any) {
        if (info != null) {
            this.service.sendData(info);
        }
    }
}