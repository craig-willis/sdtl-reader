"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const services_1 = require("./services/services");
let AppContext = 
//<p * ngFor="let key of objectKeys(content)" > {{ key }} : <span * ngIf="content[key].constructor.name != 'Object'" > {{ content[key] }}</span>
//    < /p>
class AppContext {
    constructor(service) {
        this.service = service;
        this.id = "id";
        this.messages = "messages";
        this.variable = "variable";
        this.propertyName = "propertyName";
        this.value = "value";
        this.fileName = "fileName";
        this.variables = "variables";
        this.renames = "renames";
        this.oldName = "oldName";
        this.newName = "newName";
        this.variableName = "variableName";
        this.variableRange = "variableRange";
        this.label = "label";
        this.condition = "condition";
        this.type = "$type";
        this.function = "function";
        this.expression = "expression";
        this.sourceInformation = "sourceInformation";
        this.orginalSourceText = "originalSourceText";
        this.lineNumberStart = "lineNumberStart";
        this.lineNumberEnd = "lineNumberEnd";
        this.processedSource = "ProcessdSource";
        this.objectKeys = Object.keys;
    }
    ngOnInit() {
        this.service.curdata.subscribe(data => this.content = data);
    }
};
AppContext = __decorate([
    core_1.Component({
        selector: 'my-context',
        template: `
        <div class="div4" *ngIf="(content | json) != '{}' && content[id] != null">
            <h3 class="title">Summary</h3>
            <div *ngFor="let key of objectKeys(content)">
                <p *ngIf="key != 'commands' && content[key]?.constructor.name != 'Array'">{{key}} : {{content[key]}}</p>
            </div>
            <br>
            <br>
            <br>
            <h3 *ngIf="content[messages] != null" class="emphasize">Errors</h3>
            <table>
                <tr id="table_header">
                    <th id="table_header_content">Severity</th>
                    <th id="table_header_content">Line</th>
                    <th id="table_header_content">Position</th>
                    <th id="table_header_content">Messages</th>
                </tr>
                <tr id="table_content" *ngFor="let item of content[messages]">
                    <td>
                        <div class="table-div">
                            <i *ngIf="item?.severity == 'Warning'" class="material-icons" style="float:left; font-size:20px;color:#CCCC00">warning</i>
                            <i *ngIf="item?.severity == 'Error'" class="material-icons" style="float:left; font-size:20px;color:red">error</i>
                            <p>{{item?.severity}}</p>
                        </div>
                    </td>
                    <td>{{item?.lineNumber}}</td>
                    <td>{{item?.characterPosition}}</td>
                    <td>{{item?.messageText}}</td>
                </tr>
            </table>
        </div>
        <div class="div4" *ngIf="(content | json) != '{}' && content[id] == null">
            <h3 class="title">Overview</h3>
            <p *ngIf="content[variable] != null">Variable: {{content[variable]}}</p>
            <p *ngIf="content[variableRange] != null">Variable Range: {{content[variableRange]}}</p>
            <p *ngIf="content[propertyName] != null">PropertyName: {{content[propertyName]}}</p>
            <p *ngIf="content[value] != null">Value: {{content[value]}}</p>
            <p *ngIf="content[fileName] != null">FileName: {{content[fileName]}}</p>
            <p *ngIf="content[variables] != null">Variables: {{content[variables]}}</p>
            <p *ngIf="content[renames] != null">Renames: oldName = {{content[renames][0][oldName]}} → newName= {{content[renames][0][newName]}}</p>
            <p *ngIf="content[variableName] != null">VariableName: {{content[variableName]}}</p>
            <p *ngIf="content[label] != null">Label: {{content[label]}}</p>
            <p *ngIf="content[condition] != null">Type: {{content[condition][type]}}</p>
            <p *ngIf="content[condition] != null">Label: {{content[condition][function]}}</p>
            <br>
            <p>LineNumberStart: {{content[sourceInformation]?.lineNumberStart}}</p>
            <p>LineNumberEnd: {{content[sourceInformation]?.lineNumberEnd}}</p>
            <p>OriginalSource: {{content[sourceInformation]?.originalSourceText}}</p>
            <br>
            <my-list [data]=content>Loading component</my-list>
        </div>
`
    })
    //<p * ngFor="let key of objectKeys(content)" > {{ key }} : <span * ngIf="content[key].constructor.name != 'Object'" > {{ content[key] }}</span>
    //    < /p>
    ,
    __metadata("design:paramtypes", [services_1.Services])
], AppContext);
exports.AppContext = AppContext;
//# sourceMappingURL=app.context.js.map