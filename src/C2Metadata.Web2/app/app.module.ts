﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppContext } from './app.context';
import { ArrayComponent } from './array.component';
import { ObjectList } from './objectlist';
import { Services } from './services/services';

@NgModule({
    imports: [BrowserModule],
    declarations: [AppComponent, AppContext, ArrayComponent],
    bootstrap: [AppComponent, AppContext],
    providers: [Services]
})
export class AppModule { }