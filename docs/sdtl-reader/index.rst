SDTL Reader
===========

.. toctree::
   :maxdepth: 2

   installation
   open-spss
   open-sdtl
   view-transforms
   save-json
