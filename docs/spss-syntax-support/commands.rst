Commands
-------------

Files
~~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
GET                             Implemented
SAVE                            Implemented
MATCH FILES
=================  ===========  ======

Data
~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
RECODE                          Implemented
COMPUTE                         Implemented
SORT VARIABLES
SORT CASES
=================  ===========  ======

Structure
~~~~~~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
RENAME VARIABLES                Implemented
DELETE VARIABLES                
=================  ===========  ======

Control Flow
~~~~~~~~~~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
IF                              In Progress
DO IF                           In Progress
ELSE IF                         In Progress
SELECT IF                       In Progress
DO REPEAT                       
=================  ===========  ======


Metadata
~~~~~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
TITLE                           Implemented
SUBTITLE                        Implemented
ADD VALUE LABELS
VARIABLE LABELS
FORMATS
PRINT FORMATS
ADD VALUE LABELS
MISSING VALUES
=================  ===========  ======

Misc
~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
COUNT
AGGREGATE
FREQUENCIES
LIST
TEMPORARY
EXECUTE                         Implemented
=================  ===========  ======

Macros
~~~~~~

=================  ===========  ======
Command            Description  Status
=================  ===========  ======
DEFINE
!ENDDEFINE
=================  ===========  ======
