R Syntax Support
===================

.. toctree::
   :maxdepth: 2

   architecture
   transforms
   tools
