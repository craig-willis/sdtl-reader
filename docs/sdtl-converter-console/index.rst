SDTL Converter Console Application
==================================

.. toctree::
   :maxdepth: 2

   installation
   usage
   docker

.. note::

   The console application converts SPSS and R syntax files into Structured Data Transform Language (SDTL) documentation.
